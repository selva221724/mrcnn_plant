import os
import sys
import json
import datetime
import numpy as np
import skimage.draw
import cv2
from pathlib import Path

from mrcnn.config import Config
from mrcnn import model as modellib, utils


weights_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/SYNGENTA/SOY_RM/Mask_RCNN/Training_Time_Line/Sep_20th_Sweet_Corn_training_model_ val_acc_77/mask_rcnn_balloon_0030.h5'
#
# input_dir = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/SYNGENTA/SOY_RM/FCN/data_collection/sweet_corn'
#
# out_dir = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/SYNGENTA/SOY_RM/FCN/data_collection/prediction'
#
# images =[]
# for i,j,k in os.walk(input_dir):
#     for file in k:
#         if '.jpg' in file:
#             images.append(os.path.join(i,file))

class BalloonConfig(Config):
    """Configuration for training on the toy  dataset.
    Derives from the base Config class and overrides some values.
    """
    # Give the configuration a recognizable name
    NAME = "balloon"

    # We use a GPU with 12GB memory, which can fit two images.
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 2

    # Number of classes (including background)
    NUM_CLASSES = 1 + 1  # Background + balloon

    # Number of training steps per epoch
    STEPS_PER_EPOCH = 70

    VALIDATION_STEPS = 12

    # Skip detections with < 90% confidence
    DETECTION_MIN_CONFIDENCE = 0.9


class InferenceConfig(BalloonConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
config = InferenceConfig()

model = modellib.MaskRCNN(mode="inference", config=config,
                                  model_dir=weights_path)

model.load_weights(weights_path, by_name=True)


def color_splash(image, mask):

    # Make a grayscale copy of the image. The grayscale copy still
    # has 3 RGB channels, though.
    # print(mask)
    gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) *255
    img = np.full(image.shape, 127, np.uint8)
    # Copy color pixels from the original color image where mask is set
    if mask.shape[-1] > 0:
        # We're treating all instances as one, so collapse the mask into one layer
        mask = (np.sum(mask, -1, keepdims=True) >= 1)
        # splash = np.where(mask, image, gray).astype(np.uint8)
        splash = np.where(img, mask, img).astype(np.uint8)
        gray = cv2.cvtColor(splash, cv2.COLOR_RGB2GRAY)
        ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        contours, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
        overlay = image.copy()
        cv2.drawContours(overlay, contours, -1, (0, 255, 0), 3)
        opacity = 0.4
        cv2.addWeighted(overlay, opacity, image, 1 - opacity, 0, image)
        splash = image
    # else:
    #     splash = gray.astype(np.uint8)
    else:
        contours = None
        splash = gray.astype(np.uint8)

    return contours, splash



def mask_rcnn(image):
    r = model.detect([image], verbose=1)[0]
    splash = color_splash(image, r['masks'])
    # skimage.io.imsave(out_dir+'/'+ image_name, splash)
    return splash