from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================

import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.io import imread
from osgeo import gdal, osr
from pyproj import Proj, transform
import simplekml
from pykml import parser
import itertools
from mutiple_images_detector import *

source_image='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/SYNGENTA/PBAT/borders_gaps/sweet_corn/Sweet_corn_transparent_mosaic_group1.tif'
kml_file_path ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/SYNGENTA/PBAT/borders_gaps/sweet_corn/SW_Grid.kml'


def midpoint(ptA, ptB):
   return ((ptA[0] + ptB[0]) * 0.5, (ptA[1] + ptB[1]) * 0.5)

img = imread(source_image)

def getimage(source_image):
    srcimage = gdal.Open(source_image)
    geotrans=srcimage.GetGeoTransform()
    inSRS_converter = osr.SpatialReference(wkt=srcimage.GetProjection())
    inSRS_converter.AutoIdentifyEPSG()
    epsg = inSRS_converter.GetAttrValue('AUTHORITY', 1)
    # outproj = Proj("init=EPSG:" + epsg)
    return geotrans, epsg

geotrans,epsg=getimage(source_image)

inProjch = Proj(init='epsg:32615')
outProjch = Proj(init='epsg:4326')



def set_crs(x, y,inProj ,outProj ):
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    return (ulX + (x * xDist)), (ulY + (y * yDist))

def world2Pixel(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    rtnX = geoMatrix[2]
    rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return (pixel, line)

def KMLgen(geotrans,features,KML_name):
    kml = simplekml.Kml()
    i = 0
    for row in features:
        x, y = Pixel2world(geotrans, row[0][0], row[0][1])
        g, h = Pixel2world(geotrans, row[1][0], row[1][1])
        x, y = set_crs(x, y,inProjch,outProjch)
        g, h = set_crs(g, h,inProjch,outProjch)
        line = kml.newlinestring(name="Line")
        line.coords=[(x,y),(g,h)]

        # pol.description = des[i]
        i+=1
    kml.save(KML_name)

new_coords=[]
final_area=[]
final_coor=[]

f = open(kml_file_path, "r")
docs = parser.parse(f)
try:
    doc = docs.getroot().Document.Folder

except:
    try:
        doc = docs.getroot().Document
    except:

        pass

coords = []
for place in doc.Placemark:
    x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
    if '\n' in x:
        x = x.replace('\n', ',')
        x = x.replace(' ', '')
        x = x[1:]
    else:
        x = x.replace(' ', ',')
    v = x.split(",")
    co = [(v[0], v[1]), (v[3], v[4]), (v[6], v[7]), (v[9], v[10])]
    # co = [(v[0], v[1]), (v[2], v[3]), (v[4], v[5]), (v[6], v[7])]
    coords.append(co)
    global_coord = []
    for i in range(len(coords)):
        coordinates = [(coords[i][0][0], coords[i][0][1]), (coords[i][1][0], coords[i][1][1]),
                       (coords[i][2][0], coords[i][2][1]), (coords[i][3][0], coords[i][3][1])]
        global_coord.append(coordinates)

    inProj = Proj("+init=EPSG:4326", preserve_units=True)
    outProj = Proj("+init=EPSG:32615", preserve_units=True)

    global_coord_conv = []
    for i in range(len(global_coord)):
        gcc1 = transform(inProj, outProj, global_coord[i][0][0], global_coord[i][0][1])
        gcc2 = transform(inProj, outProj, global_coord[i][1][0], global_coord[i][1][1])
        gcc3 = transform(inProj, outProj, global_coord[i][2][0], global_coord[i][2][1])
        gcc4 = transform(inProj, outProj, global_coord[i][3][0], global_coord[i][3][1])
        global_coord_conv.append([gcc1, gcc2, gcc3, gcc4])

    pixel_coord = []
    for i in range(len(global_coord_conv)):
        x, y = world2Pixel(geotrans, global_coord_conv[i][0][0], global_coord_conv[i][0][1])
        g, h = world2Pixel(geotrans, global_coord_conv[i][1][0], global_coord_conv[i][1][1])
        j, k = world2Pixel(geotrans, global_coord_conv[i][2][0], global_coord_conv[i][2][1])
        n, m = world2Pixel(geotrans, global_coord_conv[i][3][0], global_coord_conv[i][3][1])
        pixel_coord.append([(x, y), (g, h), (j, k), (n, m)])

flatten = itertools.chain.from_iterable


def sort_contours(cnts, method="left-to-right"):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0

    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True

    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1

    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))

    # return the list of sorted contours and bounding boxes
    return (cnts, boundingBoxes)

def gaps_detector(pixel_coord, row_type = 'vertical'):
    gaps =[]
    for i in range(len(pixel_coord)):
            lines = []
            area_all = []

            X = [j[0] for j in pixel_coord[i]]
            Y = [j[1] for j in pixel_coord[i]]
            x1, y1 = min(X)+10, min(Y)
            x2, y2 = max(X)-10, max(Y)

            crop_img = img[y1:y2, x1:x2]
            crop_img = cv2.cvtColor(crop_img, cv2.COLOR_RGBA2RGB)
            # hsv = cv2.cvtColor(crop_img, cv2.COLOR_RGB2HSV)
            # mask = cv2.inRange(hsv, (30, 20, 0), (80, 255, 255))
            # imask = mask > 0
            # green = np.zeros_like(crop_img, np.uint8)
            # green[imask] = crop_img[imask]
            # gray = cv2.cvtColor(green, cv2.COLOR_BGR2GRAY)
            # ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            # contours = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0]
            contours , out_img = mask_rcnn(crop_img)

            try:
                contours, _ = sort_contours(contours)

            except:
                if row_type == 'vertical':
                    x_mid = round((x1+x2) / 2)
                    gaps.append([(x_mid,y1), (x_mid,y2)])
                else:
                    y_mid = round((y1+y2) / 2)
                    gaps.append([(x1,y_mid), (x2,y_mid)])
                continue

            width, height, _ = crop_img.shape

            if row_type == 'vertical':

                x_mid = round(height/2)
                cnt_count = 0
                for cnt in contours:
                    x, y, w, h = cv2.boundingRect(cnt)
                    if cnt_count == 0:
                        lines.append([(x_mid, 0), (x_mid, y) , (x_mid,y+h)])
                    else:
                        lines.append([(x_mid,y),(x_mid,y+h)])
                    cnt_count += 1
                lines.append([(x_mid,width)])

            else:
                y_mid = round(width / 2)

                cnt_count = 0
                for cnt in contours:
                    area = cv2.contourArea(cnt)
                    area_all.append(area)
                    if area > 30:
                        x, y, w, h = cv2.boundingRect(cnt)
                        if cnt_count == 0:
                            lines.append([(0, y_mid), (x, y_mid), (x + w, y_mid)])
                        else:
                            lines.append([(x, y_mid), (x + w, y_mid)])
                        cnt_count += 1
                lines.append([(height, y_mid)])

            lines = list(flatten(lines))

            # for j in range(0,len(lines),2):
            #     cv2.line(crop_img, lines[j],lines[j+1],(255,255,255), 2)

            # cv2.imwrite('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/SYNGENTA/SOY_RM/Mask_RCNN/detections/Crop_{}.jpg'.format(i),out_img)

            for j in range(0,len(lines)-1,2):
                gaps.append([(lines[j][0]+x1,lines[j][1]+y1),(lines[j+1][0]+x1,lines[j+1][1]+y1)])

            # plt.imshow(crop_img)
    return gaps


Gaps_coord = gaps_detector(pixel_coord, row_type='horizontal')

KMLgen(geotrans, Gaps_coord, 'Gaps.kml')

# ==================================== End of Code  ===============================================================

end_time = datetime.now()
print('======================================================================================')
print('Total Duration of Over All Execution is {}'.format(end_time - start_time), "seconds")
