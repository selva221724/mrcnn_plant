import numpy as np

import cv2
from matplotlib import pyplot as plt

from skimage.io import imread
import skimage

mask = np.load('test.npy')

image = imread('/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/SYNGENTA/SOY_RM/FCN/data_collection/data/Image1218.jpg')


gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) * 255

field3d_mask = np.broadcast_to(mask > 0.3, image.shape)

img = np.full(image.shape, 127, np.uint8)
splash = np.where(img,mask,img).astype(np.uint8)


gray = cv2.cvtColor(splash, cv2.COLOR_RGB2GRAY)
ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

contours, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

overlay = image.copy()

cv2.drawContours(overlay,contours,-1,(0,255,0),3)

opacity = 0.4
cv2.addWeighted(overlay, opacity, image, 1 - opacity, 0, image)


plt.imshow(threshold)
