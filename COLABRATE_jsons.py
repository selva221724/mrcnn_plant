import cv2
import numpy as np
from skimage.io import imread
from pathlib import Path
import json
import os

img_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/SYNGENTA/SOY_RM/Mask_RCNN/datasets/sweetcorn/train'

out_put_dir = Path(img_path).parent
out_put_dir= str(out_put_dir)

def getsize(g):
    file = Path() / g  # or Path('./doc.txt')
    si = file.stat().st_size
    return si

myDictObj={}

imgs =[]
jsons=[]
for i,j,k in os.walk(img_path):
    for file in k:
        if '.jpg' in file:
            imgs.append(os.path.join(i,file))
        elif '.json' in file:
            jsons.append(os.path.join(i, file))


imgs.sort(key = str)
jsons.sort(key= str)


def json_collabrate(img,json_file,myDictObj):

    with open(json_file) as json_file:
        data = json.load(json_file)

    shape = data['shapes']
    number_of_poly = len(shape)

    points =[]
    for i in range(number_of_poly):
        point = data['shapes'][i]['points']
        points.append(point)

    size = getsize(img)
    img_name = Path(img).name

    shapes ={}
    for i in range(len(points)):
        x = [j[0] for j in points[i]]
        y = [j[1] for j in points[i]]
        shapes[i] = {"shape_attributes":{"name":"polygon",
                                         "all_points_x":x,
                                         "all_points_y":y},
                     "region_attributes":{}}


    myDictObj[img_name+str(size)] = {"fileref":"",
                                "size":size,
                                "filename":img_name,
                                "base64_img_data":"",
                                "file_attributes":{},
                                "regions": shapes}




for i in range(len(imgs)):
    json_collabrate(imgs[i],jsons[i],myDictObj)




serialized= json.dumps(myDictObj, sort_keys=True, indent=3)

with open(img_path+'/'+'via_region_data.json', 'w', encoding='utf-8') as f:
    json.dump(myDictObj, f, ensure_ascii=False, indent=2)

